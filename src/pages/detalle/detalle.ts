import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Comercio } from '../../interfaces/comercio.interface';
import { CallNumber } from '@ionic-native/call-number';
import { SMS } from '@ionic-native/sms';




@IonicPage()
@Component({
  selector: 'page-detalle',
  templateUrl: 'detalle.html',
})
export class DetallePage {
  comercio:Comercio;
  lat: number = 51.678418;
  lng: number = 7.809007;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private call:CallNumber,
              private sms:SMS
             ) {

    this.comercio= this.navParams.get("comercio");
    console.log(this.comercio);
  }

  callNumber(){ 
       let numero  = this.comercio.telefono;
       this.call.callNumber(String(numero),true).then(()=>{
        console.log('call worked!');
       }).catch((error)=>{
        console.log(JSON.stringify(error));
       });
      
  }

  sendSms(){ 
    let options ={
      replaceLineBreaks : true,
      android	: {
        intent : 'INTENT'
      }
    }
    let numero  = this.comercio.telefono;
    this.sms.send(String(numero),"Mensaje desde la app",options).then(()=>{
     console.log('SMS worked!');
    }).catch((error)=>{
        console.log(JSON.stringify(error));
    });

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetallePage');
  }

  
}

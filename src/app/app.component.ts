import { Component } from '@angular/core';
import { Platform,MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ListadoPage,RubrosPage,HomePage} from '../pages/index.paginas';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  listado = ListadoPage;
  rubros = RubrosPage;
  rootPage = HomePage;

  constructor(platform: Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen,
              private menuCtr: MenuController) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  abriPagina(pagina : any ){
    this.rootPage= pagina;
    this.menuCtr.close();
  }
}


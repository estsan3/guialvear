import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetallePage} from '../index.paginas';
import { COMERCIOS } from '../../data/data.comercios';
import { Comercio } from '../../interfaces/comercio.interface';

@IonicPage()
@Component({
  selector: 'page-listado',
  templateUrl: 'listado.html',
})
export class ListadoPage {
  comercios:Comercio[] = [];
  search:string='';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.comercios = COMERCIOS.slice(0);
    debugger
    this.search= this.navParams.get("search");
    this.search = this.search.toLowerCase();
    this.comercios = this.comercios.filter(comercio => comercio.rubro.toLowerCase().search(this.search) != -1 
                                                  || comercio.empresa.toLowerCase().search(this.search) != -1);
  }

  irDetalle(comercio:Comercio){
    this.navCtrl.push(DetallePage,{'comercio':comercio});
    console.log(comercio);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ListadoPage');
  }

}

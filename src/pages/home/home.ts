import { Component } from '@angular/core';
import { NavController,MenuController } from 'ionic-angular';
import { ListadoPage,RubrosPage} from '../index.paginas';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  rubros:any = RubrosPage;
  search :string ='';
  
  constructor(public navCtrl: NavController,
              private menuCtrl:MenuController) {
  }

  mostrarMenu(){

    this.menuCtrl.toggle();
  }
  navegarListado(){
      this.navCtrl.push(ListadoPage,{'search':this.search});
      this.search='';
  }

}

export interface Comercio {
    localidad: String;
    rubro : String;
    nombre : String;
    apellido : String;
    empresa: String;
    caracteristica: number;
    telefono: number;
    mail: String;
    direccion: String;
    nro: number;
    web: String;
    facebook: String;
    instagram : String;
    logo: String;
    descripcion: String;
}
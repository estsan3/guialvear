import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';

// Maps
import { AgmCoreModule } from '@agm/core';

// Call
import { CallNumber } from '@ionic-native/call-number';
// SMS
import { SMS } from '@ionic-native/sms';

import { 
        RubrosPage,
        ListadoPage,
        HomePage,
        DetallePage } from '../pages/index.paginas';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListadoPage,
    RubrosPage,
    DetallePage
  ],
  imports: [
    
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB1hRXIpni8kIqNPI44SY9tMVlFL3d6OWM',
      libraries: ["places"]
    }),
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListadoPage,
    RubrosPage,
    DetallePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CallNumber,
    SMS
  ]
})
export class AppModule {}
